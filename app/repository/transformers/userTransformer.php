<?php 

namespace App\repository\Transformers;
 
class userTransformer extends Transformer {

	public function transform($user)
    {
    		return [
    			'userID' 	=> $user['user_ID'],
    			'firstName' => $user['first_name'],
    			'lastName' 	=> $user['last_name'],
    			'emailID' 	=> $user['email_id'],
    			'dob' 		=> $user['date_of_birth'],
    			'gender' 	=> $user['gender'],
    			'isActive' 	=> $user['is_active'],
    			'from' 		=> $user['created_at']
    		];
    }

}