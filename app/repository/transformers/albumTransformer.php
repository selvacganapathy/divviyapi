<?php 

namespace App\repository\Transformers;
 
class albumTransformer extends Transformer {

	public function transform($album)
    {
    		return [
    			'uploadID' 	        => $album['upload_ID'],
    			'optionID'          => $album['option_ID'],
    			'categoryGenreID' 	=> $album['category_genre_ID'],
    			'uploadBy' 	        => $album['uploaded_by'],
    			'authorName' 		=> $album['artist_author_name'],
    			'albumTitle' 	    => $album['album_title_name'],
    			'albumPrice' 	    => $album['album_price'],
                'TrackName'         => $album['track_name'],
                'isActive'          => $album['is_active'],
                'from'              => $album['created_at'],
    			'albumType' 		=> $album['option_name']
    		];
    }

}