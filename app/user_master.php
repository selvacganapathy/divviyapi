<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class user_master extends Model
{
    protected $primaryKey = 'user_ID';

    protected $table = 'user_master';   

    protected $dates = ['date_of_birth'];

    
    protected $fillable = [
    	'first_name',
    	'last_name',
    	'email_id',
        'remember_token',
    	'date_of_birth',
    	'gender',
    	'user_type',
    	'origanisation',
    	'is_active'	
    ];


}
