<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return Response::json([
    	'error' => 'Forbidden'
    	],404);
});

Route::group(['prefix' => 'api/v1'],function(){

Route::get('/',function(){
	 return Response::json([
    	'error' => 'Forbidden'
    	],404);
});
	
	Route::resource('users','userController');

	Route::resource('users/login','userController@login');

	Route::resource('albums','albumController');


});