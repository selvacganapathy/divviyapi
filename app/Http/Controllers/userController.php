<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\repository\Transformers\userTransformer;
/*  FACADE */
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Response;
use Auth;
/* MODELS */
use App\user_master;
use App\user_password;

class userController extends statusCodeController
{
    
    protected $userTransformer;

    function __construct (userTransformer $userTransformer)
    {
        $this->userTransformer = $userTransformer;

        $this->middleware('auth.basic',['only' => ['store','login']]);//

    }

    public function index() 
    {
		$user = user_master::all();	

		if(!$user)
		{
			return $this ->setStatusCode(404)

                            ->responseWithError('No Users Found');
			
		}
        
        return $this->respond([
            'data' => $this->userTransformer->transformCollection($user->all())

        ]);
    		
    }

    public function show($emailID) 
    {

    	$user = user_master::where('email_id',$emailID)
							->first();	
		if(!$user)
		{
			return $this->responseNotFound('User not found');
			
		}
        return $this->respond([
            'data' => $this->userTransformer->transform($user)

            ]);
    }

    public function store()
    {
        dd('login successful');

            return $this    ->setStatusCode(422)

                            ->responseWithError('parameter failed validation for registration');
    }
    /**
     * user login 
     * get logged in user details
     */
    public function login()
    {
        if(Auth::user()->email_id)
        {
            $user = user_master::where('email_id',Auth::user()->email_id)
                                
                                ->first();  

            return $this->respond([

                'data' => $this->userTransformer->transform($user)

            ]);
        }

        return $this->responseNotFound('User not found');
    }

}
