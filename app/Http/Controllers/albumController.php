<?php

namespace App\Http\Controllers;

/*  FACADE */
use App\Http\Requests;
use App\repository\Transformers\albumTransformer;
use Illuminate\Support\Facades\Input;
use Response;
use Auth;
/* MODELS */
use App\uploadMaster;

class albumController extends statusCodeController
{
	protected $albumTransformer;

    function __construct (albumTransformer $albumTransformer)
    {
    	$this->albumTransformer = $albumTransformer;

        $this->middleware('auth.basic');
    }

    public function index() 
    {
    	if(Input::get('userID'))
    	{
	    	$albums = uploadMaster::where('uploaded_by',Input::get('userID'))
	    							->leftJoin('options_masters', function($join) {
	      									$join->on('upload_masters.option_ID', '=', 'options_masters.option_ID');
	    							})
	    						->get();	
			if($albums->isEmpty())
			{
				return $this ->setStatusCode(404)

	                            ->responseWithError('No albums Found');
				
			}
	        
	        return $this->respond([

	            'data' => $this->albumTransformer->transformCollection($albums->all())

	        ]);
	    }
	    return $this ->setStatusCode(404)

	                            ->responseWithError('No Users Found');
    }
    
    public function show() {
    	
    }

    public function store() {
    	
    }
}
