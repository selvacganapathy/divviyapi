<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Response;

class statusCodeController extends Controller
{
 	protected $statusCode = 200;

	public function getStatusCode()
	{

		return $this->statusCode;

	}
	
	public function setStatusCode($statusCode)
	{

		$this->statusCode = $statusCode;

		return $this;

	}

	public function respond($data,$headers = [])
	{

		return Response::json($data,$this->getStatusCode(),$headers);

	}

	public function responseNotFound($message = 'Not Found')
	{
		return $this->setStatusCode(404)->responseWithError($message);
	}

	public function responseWithError($message)
	{

		return $this->respond([
			'error'	=> [
				'message' => $message,
				'statusCode'	=>	$this->getStatusCode()
				]
		]);

	}
}	
?>
