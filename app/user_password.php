<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class user_password extends Model
{
    protected $primaryKey = 'pwd_ID';
    
    protected $table = 'user_password';   

    protected $fillable = [
    	'user_ID',
    	'email_id',
    	'password',
    	'user_type',
    	'remember_token',
    	'is_active'
    ];

}
