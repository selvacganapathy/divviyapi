<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class urlMaster extends Model
{
    protected $primaryKey = 'url_ID';
    
    protected $table = 'url_masters';   

    protected $fillable = [
    	'url_params',
    	'full_url',
    	'filename',
    	'upload_ID',
    	'is_active',
    ];
}
