<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class optionsMaster extends Model
{
    protected $primaryKey = 'option_ID';
    
    protected $table = 'options_masters';   

    protected $fillable = [
    	'option_name',
    	'is_active',
    ];
}
