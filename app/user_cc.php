<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class user_cc extends Model
{
    protected $primaryKey = 'user_cc_ID';
    
    protected $table = 'user_cc';   

    protected $fillable = [
    	'user_ID',
    	'card_name',
    	'card_number',
    	'security_code',
    	'sort_code',
    	'account_number',
    	'expiry_date',
    	'is_active'
    ];
}
